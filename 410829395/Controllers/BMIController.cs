﻿using _410829395.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _410829395.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        // GET: BMI
        public ActionResult Index(BMIData data)
        {
            if (data.Height < 50 || data.Height > 200)
            {
                ViewBag.HeightError = "身高請輸入50~200的數值";
            }
            if (data.Weight < 30 || data.Height > 150)
            {
                ViewBag.WeightError = "體重請輸入30~150的數值";
            }
            if (ModelState.IsValid)
            {
                var m_height = data.Height.Value / 100;
                var result = data.Weight / (m_height * m_height);

                var Level = "";

                if (result < 18.5)
                {
                    Level = "體重過輕";
                }

                else if (result >= 18.5 && result < 24)
                {
                    Level = "正常範圍";
                }

                else if (result >= 24 && result < 27)
                {
                    Level = "過重";
                }

                else if (result >= 27 && result < 30)
                {
                    Level = "輕度肥胖";
                }

                else if (result >= 30 && result < 35)
                {
                    Level = "中度肥胖";
                }

                else if (result >= 35)
                {
                    Level = "重度肥胖";
                }

                //ViewBag.H = Height;
                //ViewBag.W = Weight;

                data.Result = result;
                data.Level = Level;

            }
            return View(data);
        }
    }
}