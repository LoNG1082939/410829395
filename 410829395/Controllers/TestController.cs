﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _410829395.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["abc"] = "654";

            ViewBag.name = "DADA";

            ViewBag.A = 1;
            ViewBag.B = 2;

            ViewData["A"] = 1;
            ViewData["B"] = 2;



            return View();
        }

        public ActionResult HTML()
        {
            return View();
        }

        public ActionResult HTMLHelper()
        {
            return View();
        }

        public ActionResult Razor()
        {
            return View();
        }


    }
}