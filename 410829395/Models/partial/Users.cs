﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _410829395.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class Users
    {
    }

    public class UserMetadata
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("電子郵件")]
        [EmailAddress]
        [StringLength(50)]
        public string Email { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("密碼")]
        [StringLength(10)]
        public string Password { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("生日")]
        public Nullable<System.DateTime> Birthday { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
        public Nullable<bool> Gender { get; set; }
    }
}